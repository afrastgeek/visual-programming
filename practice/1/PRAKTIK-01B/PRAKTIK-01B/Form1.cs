﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PRAKTIK_01B
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void Hitungbtn_Click(object sender, EventArgs e)
        {
            //ambil data jenis
            string jenis;
            jenis = Jeniscbo.Text;

            //ambil data asuransi
            Boolean asuransi;
            asuransi = Asuransichk.Checked;

            //ambil data berat
            decimal berat;
            berat = Convert.ToDecimal(Berattxt.Text);

            //hitung ongkos
            decimal ongkos = 0;

            if (jenis == "Reguler")
            {
                ongkos = 1000;
            }
            else if (jenis == "Kilat")
            {
                ongkos = 2000;
            }
            else if (jenis == "ONS")
            {
                ongkos = 3000;
            }

            ongkos = ongkos * berat;

            if (asuransi)
            {
                //setiap asuransi dikenai biaya tambahan 5%
                ongkos = ongkos * 105 / 100;
            }

            //tampilkan ongkos
            Ongkostxt.Text = Convert.ToString(ongkos);
        }

        private void Resetbtn_Click(object sender, EventArgs e)
        {
            //bersihkan semua textbox
            Nomortxt.Clear();
            Kepadatxt.Clear();
            Alamattxt.Clear();

            //arahkan fokus ke nomor
            Nomortxt.Focus();
        }
    }
}
