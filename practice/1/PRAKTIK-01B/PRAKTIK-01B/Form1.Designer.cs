﻿namespace PRAKTIK_01B
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.Hitungbtn = new System.Windows.Forms.Button();
            this.Resetbtn = new System.Windows.Forms.Button();
            this.Nomortxt = new System.Windows.Forms.TextBox();
            this.Kepadatxt = new System.Windows.Forms.TextBox();
            this.Alamattxt = new System.Windows.Forms.TextBox();
            this.Berattxt = new System.Windows.Forms.TextBox();
            this.Ongkostxt = new System.Windows.Forms.TextBox();
            this.Jeniscbo = new System.Windows.Forms.ComboBox();
            this.Asuransichk = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(5, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(137, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "E.K.S.P.E.D.I.S.I P.A.K.E.T";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(313, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "JEJEN EXPRESS";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(52, 35);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Nomor";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(52, 64);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Kepada";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(52, 93);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Alamat";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(52, 154);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(36, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Jenis";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(52, 183);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(64, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Berat (Kg)";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(52, 212);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(81, 13);
            this.label9.TabIndex = 8;
            this.label9.Text = "Ongkos Kirim";
            // 
            // Hitungbtn
            // 
            this.Hitungbtn.Location = new System.Drawing.Point(87, 241);
            this.Hitungbtn.Name = "Hitungbtn";
            this.Hitungbtn.Size = new System.Drawing.Size(75, 23);
            this.Hitungbtn.TabIndex = 9;
            this.Hitungbtn.Text = "Hitung";
            this.Hitungbtn.UseVisualStyleBackColor = true;
            this.Hitungbtn.Click += new System.EventHandler(this.Hitungbtn_Click);
            // 
            // Resetbtn
            // 
            this.Resetbtn.Location = new System.Drawing.Point(193, 241);
            this.Resetbtn.Name = "Resetbtn";
            this.Resetbtn.Size = new System.Drawing.Size(75, 23);
            this.Resetbtn.TabIndex = 10;
            this.Resetbtn.Text = "Reset";
            this.Resetbtn.UseVisualStyleBackColor = true;
            this.Resetbtn.Click += new System.EventHandler(this.Resetbtn_Click);
            // 
            // Nomortxt
            // 
            this.Nomortxt.Location = new System.Drawing.Point(143, 32);
            this.Nomortxt.Name = "Nomortxt";
            this.Nomortxt.Size = new System.Drawing.Size(100, 20);
            this.Nomortxt.TabIndex = 11;
            this.Nomortxt.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // Kepadatxt
            // 
            this.Kepadatxt.Location = new System.Drawing.Point(143, 61);
            this.Kepadatxt.Name = "Kepadatxt";
            this.Kepadatxt.Size = new System.Drawing.Size(100, 20);
            this.Kepadatxt.TabIndex = 12;
            // 
            // Alamattxt
            // 
            this.Alamattxt.Location = new System.Drawing.Point(143, 87);
            this.Alamattxt.Multiline = true;
            this.Alamattxt.Name = "Alamattxt";
            this.Alamattxt.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.Alamattxt.Size = new System.Drawing.Size(216, 55);
            this.Alamattxt.TabIndex = 13;
            this.Alamattxt.TextChanged += new System.EventHandler(this.textBox3_TextChanged);
            // 
            // Berattxt
            // 
            this.Berattxt.Location = new System.Drawing.Point(143, 180);
            this.Berattxt.Name = "Berattxt";
            this.Berattxt.Size = new System.Drawing.Size(100, 20);
            this.Berattxt.TabIndex = 14;
            // 
            // Ongkostxt
            // 
            this.Ongkostxt.Location = new System.Drawing.Point(143, 209);
            this.Ongkostxt.Name = "Ongkostxt";
            this.Ongkostxt.ReadOnly = true;
            this.Ongkostxt.Size = new System.Drawing.Size(100, 20);
            this.Ongkostxt.TabIndex = 15;
            // 
            // Jeniscbo
            // 
            this.Jeniscbo.FormattingEnabled = true;
            this.Jeniscbo.Items.AddRange(new object[] {
            "Reguler",
            "Kilat",
            "ONS"});
            this.Jeniscbo.Location = new System.Drawing.Point(143, 151);
            this.Jeniscbo.Name = "Jeniscbo";
            this.Jeniscbo.Size = new System.Drawing.Size(121, 21);
            this.Jeniscbo.TabIndex = 16;
            // 
            // Asuransichk
            // 
            this.Asuransichk.AutoSize = true;
            this.Asuransichk.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Asuransichk.Checked = true;
            this.Asuransichk.CheckState = System.Windows.Forms.CheckState.Checked;
            this.Asuransichk.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Asuransichk.Location = new System.Drawing.Point(293, 155);
            this.Asuransichk.Name = "Asuransichk";
            this.Asuransichk.Size = new System.Drawing.Size(74, 17);
            this.Asuransichk.TabIndex = 17;
            this.Asuransichk.Text = "Asuransi";
            this.Asuransichk.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(408, 279);
            this.Controls.Add(this.Asuransichk);
            this.Controls.Add(this.Jeniscbo);
            this.Controls.Add(this.Ongkostxt);
            this.Controls.Add(this.Berattxt);
            this.Controls.Add(this.Alamattxt);
            this.Controls.Add(this.Kepadatxt);
            this.Controls.Add(this.Nomortxt);
            this.Controls.Add(this.Resetbtn);
            this.Controls.Add(this.Hitungbtn);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Transaksi";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button Hitungbtn;
        private System.Windows.Forms.Button Resetbtn;
        private System.Windows.Forms.TextBox Nomortxt;
        private System.Windows.Forms.TextBox Kepadatxt;
        private System.Windows.Forms.TextBox Alamattxt;
        private System.Windows.Forms.TextBox Berattxt;
        private System.Windows.Forms.TextBox Ongkostxt;
        private System.Windows.Forms.ComboBox Jeniscbo;
        private System.Windows.Forms.CheckBox Asuransichk;
    }
}

