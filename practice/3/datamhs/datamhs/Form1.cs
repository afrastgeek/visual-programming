﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace datamhs
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        DataTable table = new DataTable("data");

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        public void readFile()
        {
            table.ReadXml(@"E:\DATA UPI\Provis\datamhs\datamhs\DataMahasiswa.xml");
            dgData.DataSource = table;
        }

        public void writeFile()
        {
            table.WriteXml(@"E:\DATA UPI\Provis\datamhs\datamhs\DataMahasiswa.xml", XmlWriteMode.WriteSchema);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            table.Columns.Add("NIM", typeof(int));
            table.Columns.Add("Nama", typeof(string));
            table.Columns.Add("Nilai", typeof(string));

            readFile();
        }

        private void btnInput_Click(object sender, EventArgs e)
        {
            table.Rows.Add(tbNIM.Text, tbnama.Text, tbnilai.Text);
            dgData.DataSource = table;

            writeFile();

            tbNIM.Clear();
            tbnama.Clear();
            tbnilai.Clear();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            int selectedCount = dgData.SelectedRows.Count;
            while(selectedCount > 0) 
            {
                if(!dgData.SelectedRows[0].IsNewRow) 
                {
                    dgData.Rows.RemoveAt(dgData.SelectedRows[0].Index);
                }
                selectedCount--;
            }
            writeFile();
        }
    }
}
