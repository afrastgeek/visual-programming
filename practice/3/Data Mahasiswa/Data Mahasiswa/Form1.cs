﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Data_Mahasiswa
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        DataTable table = new DataTable("data");

        public void readFile()
        {
            table.ReadXml("..\\..\\DataMahasiswa.xml");
            dgData.DataSource = table;
        }

        public void writeFile()
        {
            table.WriteXml("..\\..\\DataMahasiswa.xml", XmlWriteMode.WriteSchema);
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            table.Columns.Add("NIM", typeof(string));
            table.Columns.Add("Nama", typeof(string));
            table.Columns.Add("Nilai", typeof(string));

            readFile();
        }

        private void btnInput_Click(object sender, EventArgs e)
        {
                table.Rows.Add(tbNIM.Text, tbNama.Text, tbNilai.Text);
            dgData.DataSource = table;

            writeFile();

            tbNIM.Clear();
            tbNama.Clear();
            tbNilai.Clear();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            int selectedCount = dgData.SelectedRows.Count;
            while (selectedCount > 0)
            {
                if (!dgData.SelectedRows[0].IsNewRow)
                {
                    dgData.Rows.RemoveAt(dgData.SelectedRows[0].Index);
                    selectedCount--;
                }
            }

            writeFile();
        }
    }
}
