﻿namespace Data_Mahasiswa
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            this.controlGroupBox = new System.Windows.Forms.GroupBox();
            this.appTitleLabel = new System.Windows.Forms.Label();
            this.nimLabel = new System.Windows.Forms.Label();
            this.namaLabel = new System.Windows.Forms.Label();
            this.nilaiLabel = new System.Windows.Forms.Label();
            this.tbNIM = new System.Windows.Forms.TextBox();
            this.tbNama = new System.Windows.Forms.TextBox();
            this.tbNilai = new System.Windows.Forms.TextBox();
            this.btnInput = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.dgData = new System.Windows.Forms.DataGridView();
            this.controlGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgData)).BeginInit();
            this.SuspendLayout();
            // 
            // controlGroupBox
            // 
            this.controlGroupBox.Controls.Add(this.btnDelete);
            this.controlGroupBox.Controls.Add(this.btnInput);
            this.controlGroupBox.Controls.Add(this.tbNilai);
            this.controlGroupBox.Controls.Add(this.tbNama);
            this.controlGroupBox.Controls.Add(this.tbNIM);
            this.controlGroupBox.Controls.Add(this.nilaiLabel);
            this.controlGroupBox.Controls.Add(this.namaLabel);
            this.controlGroupBox.Controls.Add(this.nimLabel);
            this.controlGroupBox.Controls.Add(this.appTitleLabel);
            this.controlGroupBox.Location = new System.Drawing.Point(12, 12);
            this.controlGroupBox.Name = "controlGroupBox";
            this.controlGroupBox.Size = new System.Drawing.Size(217, 180);
            this.controlGroupBox.TabIndex = 0;
            this.controlGroupBox.TabStop = false;
            // 
            // appTitleLabel
            // 
            this.appTitleLabel.AutoSize = true;
            this.appTitleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.appTitleLabel.Location = new System.Drawing.Point(6, 16);
            this.appTitleLabel.Name = "appTitleLabel";
            this.appTitleLabel.Size = new System.Drawing.Size(145, 24);
            this.appTitleLabel.TabIndex = 1;
            this.appTitleLabel.Text = "Data Mahasiswa";
            // 
            // nimLabel
            // 
            this.nimLabel.AutoSize = true;
            this.nimLabel.Location = new System.Drawing.Point(14, 72);
            this.nimLabel.Name = "nimLabel";
            this.nimLabel.Size = new System.Drawing.Size(27, 13);
            this.nimLabel.TabIndex = 1;
            this.nimLabel.Text = "NIM";
            // 
            // namaLabel
            // 
            this.namaLabel.AutoSize = true;
            this.namaLabel.Location = new System.Drawing.Point(6, 98);
            this.namaLabel.Name = "namaLabel";
            this.namaLabel.Size = new System.Drawing.Size(35, 13);
            this.namaLabel.TabIndex = 2;
            this.namaLabel.Text = "Nama";
            // 
            // nilaiLabel
            // 
            this.nilaiLabel.AutoSize = true;
            this.nilaiLabel.Location = new System.Drawing.Point(14, 124);
            this.nilaiLabel.Name = "nilaiLabel";
            this.nilaiLabel.Size = new System.Drawing.Size(27, 13);
            this.nilaiLabel.TabIndex = 3;
            this.nilaiLabel.Text = "Nilai";
            // 
            // tbNIM
            // 
            this.tbNIM.Location = new System.Drawing.Point(47, 69);
            this.tbNIM.Name = "tbNIM";
            this.tbNIM.Size = new System.Drawing.Size(156, 20);
            this.tbNIM.TabIndex = 4;
            // 
            // tbNama
            // 
            this.tbNama.Location = new System.Drawing.Point(47, 95);
            this.tbNama.Name = "tbNama";
            this.tbNama.Size = new System.Drawing.Size(156, 20);
            this.tbNama.TabIndex = 5;
            // 
            // tbNilai
            // 
            this.tbNilai.Location = new System.Drawing.Point(47, 121);
            this.tbNilai.Name = "tbNilai";
            this.tbNilai.Size = new System.Drawing.Size(156, 20);
            this.tbNilai.TabIndex = 6;
            // 
            // btnInput
            // 
            this.btnInput.Location = new System.Drawing.Point(47, 147);
            this.btnInput.Name = "btnInput";
            this.btnInput.Size = new System.Drawing.Size(75, 23);
            this.btnInput.TabIndex = 7;
            this.btnInput.Text = "Input";
            this.btnInput.UseVisualStyleBackColor = true;
            this.btnInput.Click += new System.EventHandler(this.btnInput_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(128, 147);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 23);
            this.btnDelete.TabIndex = 8;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // dgData
            // 
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgData.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.dgData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgData.DefaultCellStyle = dataGridViewCellStyle11;
            this.dgData.Location = new System.Drawing.Point(235, 12);
            this.dgData.Name = "dgData";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgData.RowHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.dgData.Size = new System.Drawing.Size(240, 180);
            this.dgData.TabIndex = 9;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(487, 204);
            this.Controls.Add(this.dgData);
            this.Controls.Add(this.controlGroupBox);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "MainForm";
            this.Text = "Data Mahasiswa";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.controlGroupBox.ResumeLayout(false);
            this.controlGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgData)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox controlGroupBox;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnInput;
        private System.Windows.Forms.TextBox tbNilai;
        private System.Windows.Forms.TextBox tbNama;
        private System.Windows.Forms.TextBox tbNIM;
        private System.Windows.Forms.Label nilaiLabel;
        private System.Windows.Forms.Label namaLabel;
        private System.Windows.Forms.Label nimLabel;
        private System.Windows.Forms.Label appTitleLabel;
        private System.Windows.Forms.DataGridView dgData;
    }
}

