﻿namespace Provis_Resto
{
    class Menu
    {
        public string Name { get; set; }
        public int Price { get; set; }

        public Menu(string name, int price)
        {
            Name = name;
            Price = price;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
