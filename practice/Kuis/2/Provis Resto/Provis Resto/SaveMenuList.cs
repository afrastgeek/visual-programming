﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Provis_Resto
{
    class SaveMenuList
    {
        public void Write(ListView.ListViewItemCollection all_item, int num_column)
        {
            StringBuilder data = new StringBuilder();

            string itemValue;
            foreach (ListViewItem item in all_item)
            {
                for (int i = 0; i < num_column; i++)
                {
                    itemValue = item.SubItems[i].Text;
                    if (i != num_column - 1) itemValue += "##";
                    data.Append(itemValue);
                }
                data.AppendLine();
            }

            SaveFileDialog saveMenu = new SaveFileDialog
            {
                FileName = "Pesanan",
                Filter = "Text Documents (*.txt)|*.txt|All Files (*.*)|*.*",
                Title = "Save Selected Menu",
            };
            saveMenu.ShowDialog();

            if (saveMenu.FileName != "") { File.WriteAllText(saveMenu.FileName, data.ToString()); }
        }
    }
}
