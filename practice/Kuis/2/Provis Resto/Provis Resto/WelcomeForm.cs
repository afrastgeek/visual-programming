﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Provis_Resto
{
    public partial class WelcomeForm : Form
    {
        public WelcomeForm()
        {
            InitializeComponent();
        }

        private void nextButton_Click(object sender, EventArgs e)
        {
            Hide(); // hide this form
            var menuForm = new MenuForm(); // create MenuForm object
            menuForm.Closed += (s, args) => Close(); // close this form when menuForm is closed.
            menuForm.Show(); // show menuForm
        }
    }
}
