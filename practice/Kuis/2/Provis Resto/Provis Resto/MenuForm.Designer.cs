﻿namespace Provis_Resto
{
    partial class MenuForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuControlGroupBox = new System.Windows.Forms.GroupBox();
            this.addButton = new System.Windows.Forms.Button();
            this.deleteButton = new System.Windows.Forms.Button();
            this.quantityNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.priceTextBox = new System.Windows.Forms.TextBox();
            this.quantityLabel = new System.Windows.Forms.Label();
            this.priceLabel = new System.Windows.Forms.Label();
            this.menuComboBox = new System.Windows.Forms.ComboBox();
            this.menuInfoListView = new System.Windows.Forms.ListView();
            this.menuColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.priceColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.quantityColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.totalColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.purchaseButton = new System.Windows.Forms.Button();
            this.menuControlGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.quantityNumericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // menuControlGroupBox
            // 
            this.menuControlGroupBox.Controls.Add(this.addButton);
            this.menuControlGroupBox.Controls.Add(this.deleteButton);
            this.menuControlGroupBox.Controls.Add(this.quantityNumericUpDown);
            this.menuControlGroupBox.Controls.Add(this.priceTextBox);
            this.menuControlGroupBox.Controls.Add(this.quantityLabel);
            this.menuControlGroupBox.Controls.Add(this.priceLabel);
            this.menuControlGroupBox.Controls.Add(this.menuComboBox);
            this.menuControlGroupBox.Location = new System.Drawing.Point(12, 12);
            this.menuControlGroupBox.Name = "menuControlGroupBox";
            this.menuControlGroupBox.Size = new System.Drawing.Size(133, 232);
            this.menuControlGroupBox.TabIndex = 0;
            this.menuControlGroupBox.TabStop = false;
            this.menuControlGroupBox.Text = "Menu";
            // 
            // addButton
            // 
            this.addButton.Location = new System.Drawing.Point(6, 174);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(121, 23);
            this.addButton.TabIndex = 2;
            this.addButton.Text = "Add";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // deleteButton
            // 
            this.deleteButton.Location = new System.Drawing.Point(6, 203);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(121, 23);
            this.deleteButton.TabIndex = 3;
            this.deleteButton.Text = "Delete";
            this.deleteButton.UseVisualStyleBackColor = true;
            this.deleteButton.Click += new System.EventHandler(this.deleteButton_Click);
            // 
            // quantityNumericUpDown
            // 
            this.quantityNumericUpDown.Location = new System.Drawing.Point(6, 123);
            this.quantityNumericUpDown.Name = "quantityNumericUpDown";
            this.quantityNumericUpDown.Size = new System.Drawing.Size(121, 20);
            this.quantityNumericUpDown.TabIndex = 6;
            // 
            // priceTextBox
            // 
            this.priceTextBox.Enabled = false;
            this.priceTextBox.Location = new System.Drawing.Point(6, 74);
            this.priceTextBox.Name = "priceTextBox";
            this.priceTextBox.Size = new System.Drawing.Size(121, 20);
            this.priceTextBox.TabIndex = 5;
            // 
            // quantityLabel
            // 
            this.quantityLabel.AutoSize = true;
            this.quantityLabel.Location = new System.Drawing.Point(6, 107);
            this.quantityLabel.Name = "quantityLabel";
            this.quantityLabel.Size = new System.Drawing.Size(46, 13);
            this.quantityLabel.TabIndex = 4;
            this.quantityLabel.Text = "Quantity";
            // 
            // priceLabel
            // 
            this.priceLabel.AutoSize = true;
            this.priceLabel.Location = new System.Drawing.Point(6, 58);
            this.priceLabel.Name = "priceLabel";
            this.priceLabel.Size = new System.Drawing.Size(31, 13);
            this.priceLabel.TabIndex = 3;
            this.priceLabel.Text = "Price";
            // 
            // menuComboBox
            // 
            this.menuComboBox.FormattingEnabled = true;
            this.menuComboBox.Location = new System.Drawing.Point(6, 24);
            this.menuComboBox.Name = "menuComboBox";
            this.menuComboBox.Size = new System.Drawing.Size(121, 21);
            this.menuComboBox.TabIndex = 2;
            this.menuComboBox.SelectedIndexChanged += new System.EventHandler(this.menuComboBox_SelectedIndexChanged);
            // 
            // menuInfoListView
            // 
            this.menuInfoListView.CheckBoxes = true;
            this.menuInfoListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.menuColumnHeader,
            this.priceColumnHeader,
            this.quantityColumnHeader,
            this.totalColumnHeader});
            this.menuInfoListView.GridLines = true;
            this.menuInfoListView.Location = new System.Drawing.Point(151, 18);
            this.menuInfoListView.Name = "menuInfoListView";
            this.menuInfoListView.Size = new System.Drawing.Size(302, 197);
            this.menuInfoListView.TabIndex = 1;
            this.menuInfoListView.UseCompatibleStateImageBehavior = false;
            this.menuInfoListView.View = System.Windows.Forms.View.Details;
            // 
            // menuColumnHeader
            // 
            this.menuColumnHeader.Text = "Menu";
            this.menuColumnHeader.Width = 120;
            // 
            // priceColumnHeader
            // 
            this.priceColumnHeader.Text = "Price";
            this.priceColumnHeader.Width = 50;
            // 
            // quantityColumnHeader
            // 
            this.quantityColumnHeader.Text = "Quantity";
            this.quantityColumnHeader.Width = 51;
            // 
            // totalColumnHeader
            // 
            this.totalColumnHeader.Text = "Total";
            // 
            // purchaseButton
            // 
            this.purchaseButton.Location = new System.Drawing.Point(151, 221);
            this.purchaseButton.Name = "purchaseButton";
            this.purchaseButton.Size = new System.Drawing.Size(302, 23);
            this.purchaseButton.TabIndex = 2;
            this.purchaseButton.Text = "Purchase";
            this.purchaseButton.UseVisualStyleBackColor = true;
            this.purchaseButton.Click += new System.EventHandler(this.purchaseButton_Click);
            // 
            // MenuForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(465, 256);
            this.Controls.Add(this.purchaseButton);
            this.Controls.Add(this.menuInfoListView);
            this.Controls.Add(this.menuControlGroupBox);
            this.Name = "MenuForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Select Menu";
            this.menuControlGroupBox.ResumeLayout(false);
            this.menuControlGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.quantityNumericUpDown)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox menuControlGroupBox;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.NumericUpDown quantityNumericUpDown;
        private System.Windows.Forms.TextBox priceTextBox;
        private System.Windows.Forms.Label quantityLabel;
        private System.Windows.Forms.Label priceLabel;
        private System.Windows.Forms.ComboBox menuComboBox;
        private System.Windows.Forms.ListView menuInfoListView;
        private System.Windows.Forms.ColumnHeader menuColumnHeader;
        private System.Windows.Forms.ColumnHeader priceColumnHeader;
        private System.Windows.Forms.ColumnHeader quantityColumnHeader;
        private System.Windows.Forms.ColumnHeader totalColumnHeader;
        private System.Windows.Forms.Button purchaseButton;
    }
}