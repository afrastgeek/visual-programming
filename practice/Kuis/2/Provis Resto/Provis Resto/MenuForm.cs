﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Provis_Resto
{
    public partial class MenuForm : Form
    {
        Menu[] menuItem =
        {
          new Menu("Mie Kuah Java", 17000),
          new Menu("Mie Kuah PHP", 13000),
          new Menu("Mie Goreng HTML", 10000),
          new Menu("Nasi Goreng Python", 12000),
          new Menu("Bubur R", 15000),
          new Menu("Bubur XML", 8000),
          new Menu("Es Teh C#", 9000),
          new Menu("Es Teh C++", 5000)
        };

        public MenuForm()
        {
            InitializeComponent();
            foreach (Menu item in menuItem)
            {
                menuComboBox.Items.Add(item);
            }
        }

        private bool IsListViewEmpty(ListView lv)
        {
            if (lv.Items.Count == 0) return true;
            return false;
        }

        private void menuComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            priceTextBox.Text = (menuComboBox.SelectedItem as Menu).Price.ToString();
        }

        private void addButton_Click(object sender, EventArgs e)
        {

            if (quantityNumericUpDown.Value == 0) { MessageBox.Show("Add Quantity"); }
            else
            {
                int price = (menuComboBox.SelectedItem as Menu).Price;
                decimal qty = quantityNumericUpDown.Value;

                ListViewItem menuSelected = new ListViewItem((menuComboBox.SelectedItem as Menu).Name.ToString());
                menuSelected.SubItems.Add(price.ToString());
                menuSelected.SubItems.Add(qty.ToString());
                menuSelected.SubItems.Add((price * qty).ToString());

                menuInfoListView.Items.Add(menuSelected);
            }
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            if (IsListViewEmpty(menuInfoListView)) { MessageBox.Show("You don't Have Menu List"); }
            else if (menuInfoListView.SelectedItems.Count == 0) { MessageBox.Show("Checklist Menu!"); }
            else
            {
                foreach (ListViewItem listItem in menuInfoListView.SelectedItems)
                {
                    menuInfoListView.Items.Remove(listItem);
                }
            }
        }

        private void purchaseButton_Click(object sender, EventArgs e)
        {
            if (IsListViewEmpty(menuInfoListView)) { MessageBox.Show("You don't Have Menu List"); }
            else
            {
                (new SaveMenuList()).Write(menuInfoListView.Items, menuInfoListView.Columns.Count);
            }
        }
    }
}
