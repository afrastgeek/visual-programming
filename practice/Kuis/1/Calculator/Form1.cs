﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculator
{
    public partial class calculator : Form
    {
        public calculator()
        {
            InitializeComponent();
        }

        private void plusnBtn_Click(object sender, EventArgs e)
        {
            double a;
            double b;
            if (double.TryParse(inputTxt1.Text, out a) && double.TryParse(inputTxt2.Text, out b))
            {
                resultTxt.Text = Convert.ToString(a + b);
            }
            else
            {
                MessageBox.Show("Invalid Input!");
            }
        }


        private void minusBtn_Click(object sender, EventArgs e)
        {
            double a;
            double b;
            if (double.TryParse(inputTxt1.Text, out a) && double.TryParse(inputTxt2.Text, out b))
            {
                resultTxt.Text = Convert.ToString(a - b);
            }
            else
            {
                MessageBox.Show("Invalid Input!");
            }
        }

        private void multiplyBtn_Click(object sender, EventArgs e)
        {
            double a;
            double b;
            if (double.TryParse(inputTxt1.Text, out a) && double.TryParse(inputTxt2.Text, out b))
            {
                resultTxt.Text = Convert.ToString(a * b);
            }
            else
            {
                MessageBox.Show("Invalid Input!");
            }
        }

        private void divideBtn_Click(object sender, EventArgs e)
        {
            double a;
            double b;
            if (double.TryParse(inputTxt1.Text, out a) && double.TryParse(inputTxt2.Text, out b) && b != 0)
            {
                resultTxt.Text = Convert.ToString(a / b);
            }
            else
            {
                MessageBox.Show("Invalid Input!");
            }
        }

        private void nextBtn_Click(object sender, EventArgs e)
        {
            inputTxt1.Text = resultTxt.Text;
            inputTxt2.Clear();
            resultTxt.Clear();
        }

        private void resetBtn_Click(object sender, EventArgs e)
        {
            inputTxt1.Clear();
            inputTxt2.Clear();
            resultTxt.Clear();
        }
    }
}
