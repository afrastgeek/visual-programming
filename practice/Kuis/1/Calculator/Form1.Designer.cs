﻿namespace Calculator
{
    partial class calculator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.inputTxt1 = new System.Windows.Forms.TextBox();
            this.inputTxt2 = new System.Windows.Forms.TextBox();
            this.plusnBtn = new System.Windows.Forms.Button();
            this.minusBtn = new System.Windows.Forms.Button();
            this.multiplyBtn = new System.Windows.Forms.Button();
            this.divideBtn = new System.Windows.Forms.Button();
            this.nextBtn = new System.Windows.Forms.Button();
            this.resetBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.resultTxt = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // inputTxt1
            // 
            this.inputTxt1.Font = new System.Drawing.Font("Microsoft Sans Serif", 32F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inputTxt1.Location = new System.Drawing.Point(12, 12);
            this.inputTxt1.Name = "inputTxt1";
            this.inputTxt1.Size = new System.Drawing.Size(106, 56);
            this.inputTxt1.TabIndex = 2;
            // 
            // inputTxt2
            // 
            this.inputTxt2.Font = new System.Drawing.Font("Microsoft Sans Serif", 32F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inputTxt2.Location = new System.Drawing.Point(12, 74);
            this.inputTxt2.Name = "inputTxt2";
            this.inputTxt2.Size = new System.Drawing.Size(106, 56);
            this.inputTxt2.TabIndex = 3;
            // 
            // plusnBtn
            // 
            this.plusnBtn.BackColor = System.Drawing.Color.LightSeaGreen;
            this.plusnBtn.ForeColor = System.Drawing.Color.White;
            this.plusnBtn.Location = new System.Drawing.Point(12, 140);
            this.plusnBtn.Name = "plusnBtn";
            this.plusnBtn.Size = new System.Drawing.Size(22, 23);
            this.plusnBtn.TabIndex = 4;
            this.plusnBtn.Text = "+";
            this.plusnBtn.UseVisualStyleBackColor = false;
            this.plusnBtn.Click += new System.EventHandler(this.plusnBtn_Click);
            // 
            // minusBtn
            // 
            this.minusBtn.BackColor = System.Drawing.Color.LightSeaGreen;
            this.minusBtn.ForeColor = System.Drawing.Color.White;
            this.minusBtn.Location = new System.Drawing.Point(40, 140);
            this.minusBtn.Name = "minusBtn";
            this.minusBtn.Size = new System.Drawing.Size(22, 23);
            this.minusBtn.TabIndex = 5;
            this.minusBtn.Text = "-";
            this.minusBtn.UseVisualStyleBackColor = false;
            this.minusBtn.Click += new System.EventHandler(this.minusBtn_Click);
            // 
            // multiplyBtn
            // 
            this.multiplyBtn.BackColor = System.Drawing.Color.LightSeaGreen;
            this.multiplyBtn.ForeColor = System.Drawing.Color.White;
            this.multiplyBtn.Location = new System.Drawing.Point(68, 140);
            this.multiplyBtn.Name = "multiplyBtn";
            this.multiplyBtn.Size = new System.Drawing.Size(22, 23);
            this.multiplyBtn.TabIndex = 6;
            this.multiplyBtn.Text = "*";
            this.multiplyBtn.UseVisualStyleBackColor = false;
            this.multiplyBtn.Click += new System.EventHandler(this.multiplyBtn_Click);
            // 
            // divideBtn
            // 
            this.divideBtn.BackColor = System.Drawing.Color.LightSeaGreen;
            this.divideBtn.ForeColor = System.Drawing.Color.White;
            this.divideBtn.Location = new System.Drawing.Point(96, 140);
            this.divideBtn.Name = "divideBtn";
            this.divideBtn.Size = new System.Drawing.Size(22, 23);
            this.divideBtn.TabIndex = 7;
            this.divideBtn.Text = "/";
            this.divideBtn.UseVisualStyleBackColor = false;
            this.divideBtn.Click += new System.EventHandler(this.divideBtn_Click);
            // 
            // nextBtn
            // 
            this.nextBtn.BackColor = System.Drawing.Color.LightSeaGreen;
            this.nextBtn.ForeColor = System.Drawing.Color.White;
            this.nextBtn.Location = new System.Drawing.Point(183, 107);
            this.nextBtn.Name = "nextBtn";
            this.nextBtn.Size = new System.Drawing.Size(41, 23);
            this.nextBtn.TabIndex = 9;
            this.nextBtn.Text = "Next";
            this.nextBtn.UseVisualStyleBackColor = false;
            this.nextBtn.Click += new System.EventHandler(this.nextBtn_Click);
            // 
            // resetBtn
            // 
            this.resetBtn.BackColor = System.Drawing.Color.LightSeaGreen;
            this.resetBtn.ForeColor = System.Drawing.Color.White;
            this.resetBtn.Location = new System.Drawing.Point(124, 107);
            this.resetBtn.Name = "resetBtn";
            this.resetBtn.Size = new System.Drawing.Size(44, 23);
            this.resetBtn.TabIndex = 8;
            this.resetBtn.Text = "Reset";
            this.resetBtn.UseVisualStyleBackColor = false;
            this.resetBtn.Click += new System.EventHandler(this.resetBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(128, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 26);
            this.label1.TabIndex = 0;
            this.label1.Text = "Hasilnya";
            // 
            // resultTxt
            // 
            this.resultTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 32F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.resultTxt.Location = new System.Drawing.Point(124, 41);
            this.resultTxt.Name = "resultTxt";
            this.resultTxt.ReadOnly = true;
            this.resultTxt.Size = new System.Drawing.Size(100, 56);
            this.resultTxt.TabIndex = 0;
            this.resultTxt.TabStop = false;
            // 
            // calculator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(234, 175);
            this.Controls.Add(this.resultTxt);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.resetBtn);
            this.Controls.Add(this.nextBtn);
            this.Controls.Add(this.divideBtn);
            this.Controls.Add(this.multiplyBtn);
            this.Controls.Add(this.minusBtn);
            this.Controls.Add(this.plusnBtn);
            this.Controls.Add(this.inputTxt2);
            this.Controls.Add(this.inputTxt1);
            this.Name = "calculator";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Simple Calculator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox inputTxt1;
        private System.Windows.Forms.TextBox inputTxt2;
        private System.Windows.Forms.Button plusnBtn;
        private System.Windows.Forms.Button minusBtn;
        private System.Windows.Forms.Button multiplyBtn;
        private System.Windows.Forms.Button divideBtn;
        private System.Windows.Forms.Button nextBtn;
        private System.Windows.Forms.Button resetBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox resultTxt;
    }
}

