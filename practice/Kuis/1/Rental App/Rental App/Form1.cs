﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Rental_App
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void brandCB_SelectedIndexChanged(object sender, EventArgs e)
        {
            string brand = Convert.ToString(brandCB.SelectedItem);
            if (brand == "Avanza" || brand == "Xenia")
            {
                priceTxt.Text = Convert.ToString(400000);
            }
            else if (brand == "APV")
            {
                priceTxt.Text = Convert.ToString(450000);
            }
            else if (brand == "Innova")
            {
                priceTxt.Text = Convert.ToString(500000);
            }
            else if (brand == "Alphard")
            {
                priceTxt.Text = Convert.ToString(1000000);
            }
        }

        private void borrowDateP_ValueChanged(object sender, EventArgs e)
        {
            DateTime borrowTime = borrowDateP.Value.Date + borrowTimeP.Value.TimeOfDay;
            DateTime returnTime = returnDateP.Value.Date + returnTimeP.Value.TimeOfDay;
            if (returnTime < borrowTime)
            {
                MessageBox.Show("Tanggal yang Anda masukkan tidak valid!");
            }
            else
            {
                TimeSpan range = returnTime - borrowTime;
                durationTxt.Text = Convert.ToString(Math.Round(range.TotalHours));
            }
        }

        private void countBtn_Click(object sender, EventArgs e)
        {
            long basePrice = 0;
            short time = 0;

            if (Int64.TryParse(priceTxt.Text, out basePrice) && Int16.TryParse(durationTxt.Text, out time))
            {
                totalPriceTxt.Text = "Rp " + Convert.ToString(basePrice * time) + ",-";
            }
            else
            {
                MessageBox.Show("Input tidak valid!");
            }
        }
    }
}
