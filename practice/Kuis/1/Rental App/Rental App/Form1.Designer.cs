﻿namespace Rental_App
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.brandCB = new System.Windows.Forms.ComboBox();
            this.borrowDateP = new System.Windows.Forms.DateTimePicker();
            this.borrowTimeP = new System.Windows.Forms.DateTimePicker();
            this.returnDateP = new System.Windows.Forms.DateTimePicker();
            this.returnTimeP = new System.Windows.Forms.DateTimePicker();
            this.priceTxt = new System.Windows.Forms.TextBox();
            this.durationTxt = new System.Windows.Forms.TextBox();
            this.countBtn = new System.Windows.Forms.Button();
            this.totalPriceTxt = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(109, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(123, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "RENTAL MOBIL GIK";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label2.Location = new System.Drawing.Point(0, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Pilihan Mobil";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(0, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Tanggal Pinjam";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(256, 75);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Pukul";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(0, 101);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(86, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Tanggal Kembali";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(256, 101);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(34, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Pukul";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(0, 130);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(73, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Biaya per Jam";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(198, 130);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(92, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Total Durasi (Jam)";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(109, 185);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(60, 13);
            this.label9.TabIndex = 8;
            this.label9.Text = "Total Biaya";
            // 
            // brandCB
            // 
            this.brandCB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.brandCB.FormattingEnabled = true;
            this.brandCB.Items.AddRange(new object[] {
            "Avanza",
            "Xenia",
            "APV",
            "Innova",
            "Alphard"});
            this.brandCB.Location = new System.Drawing.Point(235, 42);
            this.brandCB.Name = "brandCB";
            this.brandCB.Size = new System.Drawing.Size(121, 21);
            this.brandCB.TabIndex = 9;
            this.brandCB.SelectedIndexChanged += new System.EventHandler(this.brandCB_SelectedIndexChanged);
            // 
            // borrowDateP
            // 
            this.borrowDateP.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.borrowDateP.Location = new System.Drawing.Point(92, 69);
            this.borrowDateP.Name = "borrowDateP";
            this.borrowDateP.Size = new System.Drawing.Size(100, 20);
            this.borrowDateP.TabIndex = 10;
            this.borrowDateP.ValueChanged += new System.EventHandler(this.borrowDateP_ValueChanged);
            // 
            // borrowTimeP
            // 
            this.borrowTimeP.CustomFormat = "HH:mm";
            this.borrowTimeP.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.borrowTimeP.Location = new System.Drawing.Point(304, 69);
            this.borrowTimeP.Name = "borrowTimeP";
            this.borrowTimeP.ShowUpDown = true;
            this.borrowTimeP.Size = new System.Drawing.Size(52, 20);
            this.borrowTimeP.TabIndex = 11;
            this.borrowTimeP.ValueChanged += new System.EventHandler(this.borrowDateP_ValueChanged);
            // 
            // returnDateP
            // 
            this.returnDateP.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.returnDateP.Location = new System.Drawing.Point(92, 95);
            this.returnDateP.Name = "returnDateP";
            this.returnDateP.Size = new System.Drawing.Size(100, 20);
            this.returnDateP.TabIndex = 12;
            this.returnDateP.ValueChanged += new System.EventHandler(this.borrowDateP_ValueChanged);
            // 
            // returnTimeP
            // 
            this.returnTimeP.CustomFormat = "HH:mm";
            this.returnTimeP.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.returnTimeP.Location = new System.Drawing.Point(304, 95);
            this.returnTimeP.Name = "returnTimeP";
            this.returnTimeP.ShowUpDown = true;
            this.returnTimeP.Size = new System.Drawing.Size(52, 20);
            this.returnTimeP.TabIndex = 13;
            this.returnTimeP.ValueChanged += new System.EventHandler(this.borrowDateP_ValueChanged);
            // 
            // priceTxt
            // 
            this.priceTxt.Location = new System.Drawing.Point(92, 127);
            this.priceTxt.Name = "priceTxt";
            this.priceTxt.ReadOnly = true;
            this.priceTxt.Size = new System.Drawing.Size(100, 20);
            this.priceTxt.TabIndex = 14;
            this.priceTxt.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // durationTxt
            // 
            this.durationTxt.Location = new System.Drawing.Point(304, 127);
            this.durationTxt.Name = "durationTxt";
            this.durationTxt.ReadOnly = true;
            this.durationTxt.Size = new System.Drawing.Size(52, 20);
            this.durationTxt.TabIndex = 15;
            this.durationTxt.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // countBtn
            // 
            this.countBtn.Location = new System.Drawing.Point(281, 153);
            this.countBtn.Name = "countBtn";
            this.countBtn.Size = new System.Drawing.Size(75, 23);
            this.countBtn.TabIndex = 16;
            this.countBtn.Text = "Hitung";
            this.countBtn.UseVisualStyleBackColor = true;
            this.countBtn.Click += new System.EventHandler(this.countBtn_Click);
            // 
            // totalPriceTxt
            // 
            this.totalPriceTxt.Location = new System.Drawing.Point(175, 182);
            this.totalPriceTxt.Name = "totalPriceTxt";
            this.totalPriceTxt.ReadOnly = true;
            this.totalPriceTxt.Size = new System.Drawing.Size(100, 20);
            this.totalPriceTxt.TabIndex = 17;
            this.totalPriceTxt.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(363, 209);
            this.Controls.Add(this.totalPriceTxt);
            this.Controls.Add(this.countBtn);
            this.Controls.Add(this.durationTxt);
            this.Controls.Add(this.priceTxt);
            this.Controls.Add(this.returnTimeP);
            this.Controls.Add(this.returnDateP);
            this.Controls.Add(this.borrowTimeP);
            this.Controls.Add(this.borrowDateP);
            this.Controls.Add(this.brandCB);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Rental App";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox brandCB;
        private System.Windows.Forms.DateTimePicker borrowDateP;
        private System.Windows.Forms.DateTimePicker borrowTimeP;
        private System.Windows.Forms.DateTimePicker returnDateP;
        private System.Windows.Forms.DateTimePicker returnTimeP;
        private System.Windows.Forms.TextBox priceTxt;
        private System.Windows.Forms.TextBox durationTxt;
        private System.Windows.Forms.Button countBtn;
        private System.Windows.Forms.TextBox totalPriceTxt;
    }
}

