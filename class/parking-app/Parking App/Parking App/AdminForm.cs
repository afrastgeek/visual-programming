﻿using System;
using System.Windows.Forms;
using System.IO;

namespace Parking_App
{
    public partial class AdminForm : Form
    {
        public AdminForm()
        {
            InitializeComponent();
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            MainForm mf = Application.OpenForms["MainForm"] as MainForm;
            if (mf != null) mf.Show();
            Close();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MainForm mf = Application.OpenForms["MainForm"] as MainForm;
            if (mf != null) mf.Close();
            Close();
        }

        private void AdminForm_Load(object sender, EventArgs e)
        {
            string defaultPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\ParkingApp-transaction.csv";

            if (defaultPath != "")
            {
                // Baca file dan simpan textnya di variabel data
                if (File.Exists(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "ParkingApp-transaction.csv")))
                {
                    using (StringReader data = new StringReader(File.ReadAllText(defaultPath)))
                    {
                        string row;
                        while ((row = data.ReadLine()) != null)
                        {
                            string[] columns = row.Split(',');

                            // buat objek ListViewItem, diisi dengan kolom pertama
                            ListViewItem lvi = new ListViewItem(columns[0]);

                            // isi dengan kolom selanjutnya
                            for (int i = 1; i <= 6; i++)
                            {
                                lvi.SubItems.Add(columns[i]);
                            }

                            // tambahkan objek ListViewItem ke ListView
                            listView1.Items.Add(lvi);
                        }
                    }
                }
            }
        }

        private void mainMenuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            backButton_Click(sender, e);
        }

        private void AdminForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Alt && e.KeyCode == Keys.F4) backButton_Click(sender, e);
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Parking_App.AutoClosingMessageBox.Show("help text here", "Bantuan", 2000);
        }
    }
}
