﻿namespace Parking_App
{
    partial class MotorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.helpBtn = new System.Windows.Forms.Button();
            this.masukBtn = new System.Windows.Forms.Button();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.mainMenuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.motorcycleBox = new System.Windows.Forms.PictureBox();
            this.timeLabel = new System.Windows.Forms.Label();
            this.dialogInfo = new System.Windows.Forms.Label();
            this.vehicleNumberTextBox = new System.Windows.Forms.TextBox();
            this.vehicleNumberLabel = new System.Windows.Forms.Label();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.menuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.motorcycleBox)).BeginInit();
            this.SuspendLayout();
            // 
            // helpBtn
            // 
            this.helpBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.helpBtn.Location = new System.Drawing.Point(143, 290);
            this.helpBtn.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.helpBtn.Name = "helpBtn";
            this.helpBtn.Size = new System.Drawing.Size(99, 35);
            this.helpBtn.TabIndex = 5;
            this.helpBtn.Text = "&Help";
            this.helpBtn.UseVisualStyleBackColor = true;
            this.helpBtn.Click += new System.EventHandler(this.helpBtn_Click);
            // 
            // masukBtn
            // 
            this.masukBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.masukBtn.Location = new System.Drawing.Point(117, 230);
            this.masukBtn.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.masukBtn.Name = "masukBtn";
            this.masukBtn.Size = new System.Drawing.Size(150, 50);
            this.masukBtn.TabIndex = 4;
            this.masukBtn.Text = "&Masuk";
            this.masukBtn.UseVisualStyleBackColor = true;
            this.masukBtn.Click += new System.EventHandler(this.masukBtn_Click);
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mainMenuToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Padding = new System.Windows.Forms.Padding(8, 4, 0, 4);
            this.menuStrip.Size = new System.Drawing.Size(384, 27);
            this.menuStrip.TabIndex = 4;
            this.menuStrip.Text = "menuStrip";
            this.menuStrip.Visible = false;
            // 
            // mainMenuToolStripMenuItem
            // 
            this.mainMenuToolStripMenuItem.Name = "mainMenuToolStripMenuItem";
            this.mainMenuToolStripMenuItem.Size = new System.Drawing.Size(80, 19);
            this.mainMenuToolStripMenuItem.Text = "&Main Menu";
            this.mainMenuToolStripMenuItem.Click += new System.EventHandler(this.mainMenuToolStripMenuItem_Click);
            // 
            // motorcycleBox
            // 
            this.motorcycleBox.Image = global::Parking_App.Properties.Resources.motorcycle;
            this.motorcycleBox.Location = new System.Drawing.Point(113, 49);
            this.motorcycleBox.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.motorcycleBox.Name = "motorcycleBox";
            this.motorcycleBox.Size = new System.Drawing.Size(158, 100);
            this.motorcycleBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.motorcycleBox.TabIndex = 6;
            this.motorcycleBox.TabStop = false;
            // 
            // timeLabel
            // 
            this.timeLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.timeLabel.AutoSize = true;
            this.timeLabel.Location = new System.Drawing.Point(213, 9);
            this.timeLabel.Name = "timeLabel";
            this.timeLabel.Size = new System.Drawing.Size(159, 22);
            this.timeLabel.TabIndex = 1;
            this.timeLabel.Text = "Cur Time Placeholder";
            this.timeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dialogInfo
            // 
            this.dialogInfo.AutoSize = true;
            this.dialogInfo.Font = new System.Drawing.Font("Montserrat", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dialogInfo.Location = new System.Drawing.Point(12, 9);
            this.dialogInfo.Name = "dialogInfo";
            this.dialogInfo.Size = new System.Drawing.Size(140, 22);
            this.dialogInfo.TabIndex = 0;
            this.dialogInfo.Text = "Kendaraan Roda 2";
            // 
            // vehicleNumberTextBox
            // 
            this.vehicleNumberTextBox.Font = new System.Drawing.Font("Montserrat Light", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vehicleNumberTextBox.Location = new System.Drawing.Point(107, 185);
            this.vehicleNumberTextBox.Name = "vehicleNumberTextBox";
            this.vehicleNumberTextBox.Size = new System.Drawing.Size(170, 34);
            this.vehicleNumberTextBox.TabIndex = 3;
            this.vehicleNumberTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // vehicleNumberLabel
            // 
            this.vehicleNumberLabel.AutoSize = true;
            this.vehicleNumberLabel.Location = new System.Drawing.Point(87, 160);
            this.vehicleNumberLabel.Name = "vehicleNumberLabel";
            this.vehicleNumberLabel.Size = new System.Drawing.Size(211, 22);
            this.vehicleNumberLabel.TabIndex = 2;
            this.vehicleNumberLabel.Text = "Nomor Kendaraan (opsional):";
            // 
            // timer
            // 
            this.timer.Enabled = true;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // MotorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(384, 361);
            this.ControlBox = false;
            this.Controls.Add(this.menuStrip);
            this.Controls.Add(this.vehicleNumberLabel);
            this.Controls.Add(this.vehicleNumberTextBox);
            this.Controls.Add(this.dialogInfo);
            this.Controls.Add(this.timeLabel);
            this.Controls.Add(this.motorcycleBox);
            this.Controls.Add(this.helpBtn);
            this.Controls.Add(this.masukBtn);
            this.Font = new System.Drawing.Font("Montserrat Light", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.KeyPreview = true;
            this.MainMenuStrip = this.menuStrip;
            this.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MotorForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Motor - Parking App";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MotorForm_FormClosing);
            this.Load += new System.EventHandler(this.MotorForm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MotorForm_KeyDown);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.motorcycleBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button helpBtn;
        private System.Windows.Forms.Button masukBtn;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem mainMenuToolStripMenuItem;
        private System.Windows.Forms.PictureBox motorcycleBox;
        private System.Windows.Forms.Label timeLabel;
        private System.Windows.Forms.Label dialogInfo;
        private System.Windows.Forms.TextBox vehicleNumberTextBox;
        private System.Windows.Forms.Label vehicleNumberLabel;
        private System.Windows.Forms.Timer timer;
    }
}