﻿namespace Parking_App
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.administratorButton = new System.Windows.Forms.Button();
            this.paymentButton = new System.Windows.Forms.Button();
            this.applicationNameLabel = new System.Windows.Forms.Label();
            this.welcomeTextLabel = new System.Windows.Forms.Label();
            this.carBox = new System.Windows.Forms.PictureBox();
            this.motorcycleBox = new System.Windows.Forms.PictureBox();
            this.motorcycleButton = new System.Windows.Forms.Button();
            this.carButton = new System.Windows.Forms.Button();
            this.fIleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.windowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.roda2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.roda4ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pembayaranToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.administratorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.appNameLabel = new System.Windows.Forms.Label();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.carBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.motorcycleBox)).BeginInit();
            this.menuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // administratorButton
            // 
            this.administratorButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.administratorButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.administratorButton.Location = new System.Drawing.Point(225, 312);
            this.administratorButton.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.administratorButton.Name = "administratorButton";
            this.administratorButton.Size = new System.Drawing.Size(134, 35);
            this.administratorButton.TabIndex = 4;
            this.administratorButton.Text = "&Administrator";
            this.toolTip.SetToolTip(this.administratorButton, "Lihat data transaksi dan konfigurasi aplikasi");
            this.administratorButton.UseVisualStyleBackColor = true;
            this.administratorButton.Click += new System.EventHandler(this.administratorButton_Click);
            // 
            // paymentButton
            // 
            this.paymentButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.paymentButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.paymentButton.Location = new System.Drawing.Point(225, 267);
            this.paymentButton.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.paymentButton.Name = "paymentButton";
            this.paymentButton.Size = new System.Drawing.Size(134, 35);
            this.paymentButton.TabIndex = 3;
            this.paymentButton.Text = "&Pembayaran";
            this.toolTip.SetToolTip(this.paymentButton, "Menu untuk Gerbang Keluar Parkir");
            this.paymentButton.UseVisualStyleBackColor = true;
            this.paymentButton.Click += new System.EventHandler(this.paymentButton_Click);
            // 
            // applicationNameLabel
            // 
            this.applicationNameLabel.AutoSize = true;
            this.applicationNameLabel.Font = new System.Drawing.Font("Montserrat", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.applicationNameLabel.ForeColor = System.Drawing.Color.DarkGray;
            this.applicationNameLabel.Location = new System.Drawing.Point(681, 0);
            this.applicationNameLabel.Name = "applicationNameLabel";
            this.applicationNameLabel.Size = new System.Drawing.Size(97, 22);
            this.applicationNameLabel.TabIndex = 8;
            this.applicationNameLabel.Text = "Parking App";
            this.applicationNameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // welcomeTextLabel
            // 
            this.welcomeTextLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.welcomeTextLabel.AutoSize = true;
            this.welcomeTextLabel.Font = new System.Drawing.Font("Montserrat Light", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.welcomeTextLabel.Location = new System.Drawing.Point(77, 43);
            this.welcomeTextLabel.Name = "welcomeTextLabel";
            this.welcomeTextLabel.Size = new System.Drawing.Size(430, 29);
            this.welcomeTextLabel.TabIndex = 0;
            this.welcomeTextLabel.Text = "Silahkan pilih mode aplikasi untuk memulai.";
            this.welcomeTextLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // carBox
            // 
            this.carBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.carBox.Image = global::Parking_App.Properties.Resources.car;
            this.carBox.Location = new System.Drawing.Point(332, 99);
            this.carBox.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.carBox.Name = "carBox";
            this.carBox.Size = new System.Drawing.Size(215, 100);
            this.carBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.carBox.TabIndex = 7;
            this.carBox.TabStop = false;
            this.toolTip.SetToolTip(this.carBox, "Menu untuk Gerbang Kendaraan Roda 4");
            this.carBox.Click += new System.EventHandler(this.carBox_Click);
            // 
            // motorcycleBox
            // 
            this.motorcycleBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.motorcycleBox.Image = global::Parking_App.Properties.Resources.motorcycle;
            this.motorcycleBox.Location = new System.Drawing.Point(73, 99);
            this.motorcycleBox.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.motorcycleBox.Name = "motorcycleBox";
            this.motorcycleBox.Size = new System.Drawing.Size(158, 100);
            this.motorcycleBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.motorcycleBox.TabIndex = 5;
            this.motorcycleBox.TabStop = false;
            this.toolTip.SetToolTip(this.motorcycleBox, "Menu untuk Gerbang Kendaraan Roda 2");
            this.motorcycleBox.Click += new System.EventHandler(this.motorcycleBox_Click);
            // 
            // motorcycleButton
            // 
            this.motorcycleButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.motorcycleButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.motorcycleButton.Location = new System.Drawing.Point(115, 207);
            this.motorcycleButton.Name = "motorcycleButton";
            this.motorcycleButton.Size = new System.Drawing.Size(75, 35);
            this.motorcycleButton.TabIndex = 1;
            this.motorcycleButton.Text = "Roda &2";
            this.toolTip.SetToolTip(this.motorcycleButton, "Menu untuk Gerbang Kendaraan Roda 2");
            this.motorcycleButton.UseVisualStyleBackColor = true;
            this.motorcycleButton.Click += new System.EventHandler(this.motorcycleBox_Click);
            // 
            // carButton
            // 
            this.carButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.carButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.carButton.Location = new System.Drawing.Point(402, 207);
            this.carButton.Name = "carButton";
            this.carButton.Size = new System.Drawing.Size(75, 35);
            this.carButton.TabIndex = 2;
            this.carButton.Text = "Roda &4";
            this.toolTip.SetToolTip(this.carButton, "Menu untuk Gerbang Kendaraan Roda 4");
            this.carButton.UseVisualStyleBackColor = true;
            this.carButton.Click += new System.EventHandler(this.carBox_Click);
            // 
            // fIleToolStripMenuItem
            // 
            this.fIleToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fIleToolStripMenuItem.Name = "fIleToolStripMenuItem";
            this.fIleToolStripMenuItem.Size = new System.Drawing.Size(41, 23);
            this.fIleToolStripMenuItem.Text = "&File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.ShortcutKeyDisplayString = "Alt + F4";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(154, 24);
            this.exitToolStripMenuItem.Text = "&Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.helpToolStripMenuItem1,
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(49, 23);
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // helpToolStripMenuItem1
            // 
            this.helpToolStripMenuItem1.Name = "helpToolStripMenuItem1";
            this.helpToolStripMenuItem1.Size = new System.Drawing.Size(138, 24);
            this.helpToolStripMenuItem1.Text = "View &Help";
            this.helpToolStripMenuItem1.Click += new System.EventHandler(this.helpToolStripMenuItem1_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(138, 24);
            this.aboutToolStripMenuItem.Text = "&About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // menuStrip
            // 
            this.menuStrip.BackColor = System.Drawing.Color.WhiteSmoke;
            this.menuStrip.Font = new System.Drawing.Font("Montserrat Light", 9F);
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fIleToolStripMenuItem,
            this.windowToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Padding = new System.Windows.Forms.Padding(8, 4, 0, 4);
            this.menuStrip.Size = new System.Drawing.Size(584, 31);
            this.menuStrip.TabIndex = 5;
            this.menuStrip.Text = "menuStrip";
            // 
            // windowToolStripMenuItem
            // 
            this.windowToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.roda2ToolStripMenuItem,
            this.roda4ToolStripMenuItem,
            this.pembayaranToolStripMenuItem,
            this.administratorToolStripMenuItem});
            this.windowToolStripMenuItem.Name = "windowToolStripMenuItem";
            this.windowToolStripMenuItem.Size = new System.Drawing.Size(69, 23);
            this.windowToolStripMenuItem.Text = "&Window";
            // 
            // roda2ToolStripMenuItem
            // 
            this.roda2ToolStripMenuItem.Name = "roda2ToolStripMenuItem";
            this.roda2ToolStripMenuItem.Size = new System.Drawing.Size(159, 24);
            this.roda2ToolStripMenuItem.Text = "Roda &2";
            this.roda2ToolStripMenuItem.Click += new System.EventHandler(this.roda2ToolStripMenuItem_Click);
            // 
            // roda4ToolStripMenuItem
            // 
            this.roda4ToolStripMenuItem.Name = "roda4ToolStripMenuItem";
            this.roda4ToolStripMenuItem.Size = new System.Drawing.Size(159, 24);
            this.roda4ToolStripMenuItem.Text = "Roda &4";
            this.roda4ToolStripMenuItem.Click += new System.EventHandler(this.roda4ToolStripMenuItem_Click);
            // 
            // pembayaranToolStripMenuItem
            // 
            this.pembayaranToolStripMenuItem.Name = "pembayaranToolStripMenuItem";
            this.pembayaranToolStripMenuItem.Size = new System.Drawing.Size(159, 24);
            this.pembayaranToolStripMenuItem.Text = "&Pembayaran";
            this.pembayaranToolStripMenuItem.Click += new System.EventHandler(this.pembayaranToolStripMenuItem_Click);
            // 
            // administratorToolStripMenuItem
            // 
            this.administratorToolStripMenuItem.Name = "administratorToolStripMenuItem";
            this.administratorToolStripMenuItem.Size = new System.Drawing.Size(159, 24);
            this.administratorToolStripMenuItem.Text = "&Administrator";
            this.administratorToolStripMenuItem.Click += new System.EventHandler(this.administratorToolStripMenuItem_Click);
            // 
            // appNameLabel
            // 
            this.appNameLabel.AutoSize = true;
            this.appNameLabel.BackColor = System.Drawing.Color.WhiteSmoke;
            this.appNameLabel.Font = new System.Drawing.Font("Montserrat", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.appNameLabel.Location = new System.Drawing.Point(475, 3);
            this.appNameLabel.Name = "appNameLabel";
            this.appNameLabel.Size = new System.Drawing.Size(97, 22);
            this.appNameLabel.TabIndex = 9;
            this.appNameLabel.Text = "Parking App";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(584, 361);
            this.Controls.Add(this.appNameLabel);
            this.Controls.Add(this.carButton);
            this.Controls.Add(this.motorcycleButton);
            this.Controls.Add(this.welcomeTextLabel);
            this.Controls.Add(this.applicationNameLabel);
            this.Controls.Add(this.carBox);
            this.Controls.Add(this.motorcycleBox);
            this.Controls.Add(this.paymentButton);
            this.Controls.Add(this.administratorButton);
            this.Controls.Add(this.menuStrip);
            this.Font = new System.Drawing.Font("Montserrat Light", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MainMenuStrip = this.menuStrip;
            this.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Parking App";
            ((System.ComponentModel.ISupportInitialize)(this.carBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.motorcycleBox)).EndInit();
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button administratorButton;
        private System.Windows.Forms.Button paymentButton;
        private System.Windows.Forms.PictureBox motorcycleBox;
        private System.Windows.Forms.PictureBox carBox;
        private System.Windows.Forms.Label applicationNameLabel;
        private System.Windows.Forms.Label welcomeTextLabel;
        private System.Windows.Forms.Button motorcycleButton;
        private System.Windows.Forms.Button carButton;
        private System.Windows.Forms.ToolStripMenuItem fIleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.Label appNameLabel;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem windowToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem roda2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem roda4ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pembayaranToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem administratorToolStripMenuItem;
        private System.Windows.Forms.ToolTip toolTip;
    }
}

