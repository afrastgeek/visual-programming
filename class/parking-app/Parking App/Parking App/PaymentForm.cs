﻿using System;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Parking_App
{
    public partial class PaymentForm : Form
    {
        public PaymentForm()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            MainForm mf = Application.OpenForms["MainForm"] as MainForm;
            if (mf != null) mf.Show();
            Close();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MainForm mf = Application.OpenForms["MainForm"] as MainForm;
            if (mf != null) mf.Close();
            Close();
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Parking_App.AutoClosingMessageBox.Show("help text here", "Bantuan", 2000);
        }

        private void cariButton_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(vehicleNumberTextBox.Text)) { return; }

            string id = vehicleNumberTextBox.Text;
            StringBuilder data = new StringBuilder();

            if (File.Exists(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "ParkingApp-transaction.csv")))
            {
                using (StringReader saved = new StringReader(File.ReadAllText(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "ParkingApp-transaction.csv"))))
                {
                    string row;

                    while ((row = saved.ReadLine()) != null)
                    {
                        string[] columns = row.Split(',');
                        if (columns[2] == id || columns[3] == id)
                        {
                            DateTime startTime = DateTime.Now;
                            DateTime endTime = DateTime.Now;

                            DateTime.TryParse(columns[0], out startTime);
                            columns[1] = endTime.ToString();

                            columns[5] = Convert.ToString((new Transaction()).calculateParkingCost(startTime, endTime, columns[4]));

                            // insert to listview as sub item
                            resultListView.Items[0].SubItems.Add(startTime.ToString());
                            resultListView.Items[1].SubItems.Add(endTime.ToString());
                            resultListView.Items[2].SubItems.Add(columns[2]);
                            resultListView.Items[3].SubItems.Add(columns[3]);
                            resultListView.Items[4].SubItems.Add(columns[4]);
                            resultListView.Items[5].SubItems.Add(columns[5]);

                            errorLabel.Hide();
                            resultListView.Show();
                            bayarButton.Show();
                        }
                        else { if (!resultListView.Visible) { errorLabel.Show(); } }

                        data.Append(string.Join(",", columns));
                        data.AppendLine();
                    }
                }
            }

            File.WriteAllText(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "ParkingApp-transaction.csv"), data.ToString());
        }

        private void bayarButton_Click(object sender, EventArgs e)
        {
            vehicleNumberTextBox.Clear();
            resultListView.Hide();
            bayarButton.Hide();

            string id = vehicleNumberTextBox.Text;
            StringBuilder data = new StringBuilder();
            if (File.Exists(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "ParkingApp-transaction.csv")))
            {
                using (StringReader saved = new StringReader(File.ReadAllText(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "ParkingApp-transaction.csv"))))
                {
                    string row;

                    while ((row = saved.ReadLine()) != null)
                    {
                        string[] columns = row.Split(',');
                        if (columns[2] == id || columns[3] == id)
                        {
                            columns[6] = "paid";
                        }

                        data.Append(string.Join(",", columns));
                        data.AppendLine();
                    }
                }
            }

            File.WriteAllText(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "ParkingApp-transaction.csv"), data.ToString());
        }

        private void PaymentForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Alt && e.KeyCode == Keys.F4) backButton_Click(sender, e);
        }

        private void mainMenuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            backButton_Click(sender, e);
        }

        private void vehicleNumberTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) { cariButton_Click(this, new EventArgs()); }
        }
    }
}
