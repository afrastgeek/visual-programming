﻿using System;
using System.Text;
using System.IO;

namespace Parking_App
{
    class Transaction
    {
        DateTime waktu;
        int id;
        string plat;
        string type;
        Int64 price;

        public void masuk(DateTime waktu, int id, string plat, string type, Int64 price)
        {
            this.waktu = waktu;
            this.id = id;
            this.plat = plat;
            this.type = type;
            this.price = price;

            StringBuilder data = new StringBuilder();

            if (File.Exists(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "ParkingApp-transaction.csv")))
            {
                // Baca file dan simpan textnya di variabel saved, kemudian masukkan ke data
                using (StringReader saved = new StringReader(File.ReadAllText(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "ParkingApp-transaction.csv"))))
                {
                    string row;
                    while ((row = saved.ReadLine()) != null)
                    {
                        data.Append(row);
                        data.AppendLine();
                    }
                }
            }

            // write to file
            data.Append(waktu.ToString() + ",," + id.ToString() + "," + plat + "," + type + "," + price.ToString() + ",unpaid");
            data.AppendLine();

            File.WriteAllText(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "ParkingApp-transaction.csv"), data.ToString());
        }

        public double calculateParkingCost(DateTime inTime, DateTime outTime, String vehicleType)
        {
            TimeSpan range = outTime - inTime;
            double totalTime = Math.Round(range.TotalHours);
            if (totalTime < 1) totalTime = 1;
            if (vehicleType == "Motor")
            {
                return totalTime * 1500;
            }
            else if (vehicleType == "Mobil")
            {
                return totalTime * 2000;
            }
            return 0;
        }
    }
}
