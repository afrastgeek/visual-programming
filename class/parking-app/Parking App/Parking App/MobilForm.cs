﻿using System;
using System.Windows.Forms;

namespace Parking_App
{
    public partial class MobilForm : Form
    {
        private bool _altF4Pressed;

        public MobilForm()
        {
            InitializeComponent();
        }

        private void MobilForm_Load(object sender, EventArgs e)
        {
            timeLabel.Text = DateTime.Now.ToString();
        }

        private void helpBtn_Click(object sender, EventArgs e)
        {
            Parking_App.AutoClosingMessageBox.Show("help text here", "Bantuan", 2000);
        }

        private void menuStrip_KeyDown(object sender, KeyEventArgs e)
        {
            
        }

        private void mainMenuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MainForm mf = Application.OpenForms["MainForm"] as MainForm;
            if (mf != null) mf.Show();
            Close();
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            timeLabel.Text = DateTime.Now.ToString();
        }

        private void masukBtn_Click(object sender, EventArgs e)
        {
            Int32 id = 2000000 + (new Random()).Next(1, 999999);

            (new Transaction()).masuk(DateTime.Now, id, vehicleNumberTextBox.Text, "Mobil", 0);

            Parking_App.AutoClosingMessageBox.Show("Nomor Tiket Anda: " + id, "Tiket Mobil" + vehicleNumberTextBox.Text, 3000);

            vehicleNumberTextBox.Clear();
        }

        private void MobilForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_altF4Pressed)
            {
                if (e.CloseReason == CloseReason.UserClosing)
                    e.Cancel = true;
                _altF4Pressed = false;
                mainMenuToolStripMenuItem_Click(sender, e);
            }
        }

        private void MobilForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Alt) menuStrip.Visible = !menuStrip.Visible;
            if (e.Alt && e.KeyCode == Keys.F4)
                _altF4Pressed = true;
        }
    }
}
