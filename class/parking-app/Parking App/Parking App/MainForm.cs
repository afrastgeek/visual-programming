﻿using System;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace Parking_App
{
    public partial class MainForm : Form
    {
        [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]
        private static extern IntPtr CreateRoundRectRgn
        (
            int nLeftRect, // x-coordinate of upper-left corner
            int nTopRect, // y-coordinate of upper-left corner
            int nRightRect, // x-coordinate of lower-right corner
            int nBottomRect, // y-coordinate of lower-right corner
            int nWidthEllipse, // height of ellipse
            int nHeightEllipse // width of ellipse
        );

        public MainForm()
        {
            InitializeComponent();
            Region = System.Drawing.Region.FromHrgn(CreateRoundRectRgn(0, 0, Width, Height, 20, 20));
        }

        private void administratorButton_Click(object sender, EventArgs e)
        {
            (new AdminForm()).Show();
            Hide();
        }

        private void paymentButton_Click(object sender, EventArgs e)
        {
            (new PaymentForm()).Show();
            Hide();
        }

        private void carBox_Click(object sender, EventArgs e)
        {
            (new MobilForm()).Show();
            Hide();
        }

        private void motorcycleBox_Click(object sender, EventArgs e)
        {
            (new MotorForm()).Show();
            Hide();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void helpToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Parking_App.AutoClosingMessageBox.Show("help text here", "Bantuan", 2000);
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Parking_App.AutoClosingMessageBox.Show("about text here", "Tentang", 2000);
        }

        private void roda2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            motorcycleBox_Click(sender, e);
        }

        private void roda4ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            carBox_Click(sender, e);
        }

        private void pembayaranToolStripMenuItem_Click(object sender, EventArgs e)
        {
            paymentButton_Click(sender, e);
        }

        private void administratorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            administratorButton_Click(sender, e);
        }
    }
}
