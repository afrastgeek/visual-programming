﻿namespace Parking_App
{
    partial class PaymentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.ListViewItem listViewItem19 = new System.Windows.Forms.ListViewItem(new string[] {
            "Waktu Datang"}, -1, System.Drawing.Color.Empty, System.Drawing.Color.Empty, new System.Drawing.Font("Montserrat Light", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0))));
            System.Windows.Forms.ListViewItem listViewItem20 = new System.Windows.Forms.ListViewItem(new string[] {
            "Waktu Keluar"}, -1, System.Drawing.Color.Empty, System.Drawing.Color.Empty, new System.Drawing.Font("Montserrat Light", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0))));
            System.Windows.Forms.ListViewItem listViewItem21 = new System.Windows.Forms.ListViewItem(new string[] {
            "ID Tiket"}, -1, System.Drawing.Color.Empty, System.Drawing.Color.Empty, new System.Drawing.Font("Montserrat Light", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0))));
            System.Windows.Forms.ListViewItem listViewItem22 = new System.Windows.Forms.ListViewItem(new string[] {
            "Nomor Kendaraan"}, -1, System.Drawing.Color.Empty, System.Drawing.Color.Empty, new System.Drawing.Font("Montserrat Light", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0))));
            System.Windows.Forms.ListViewItem listViewItem23 = new System.Windows.Forms.ListViewItem(new string[] {
            "Jenis Kendaraan"}, -1, System.Drawing.Color.Empty, System.Drawing.Color.Empty, new System.Drawing.Font("Montserrat Light", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0))));
            System.Windows.Forms.ListViewItem listViewItem24 = new System.Windows.Forms.ListViewItem(new string[] {
            "Total Bayar"}, -1, System.Drawing.Color.Empty, System.Drawing.Color.Empty, new System.Drawing.Font("Montserrat", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0))));
            this.bayarButton = new System.Windows.Forms.Button();
            this.cariButton = new System.Windows.Forms.Button();
            this.backButton = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mainMenuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.appNameLabel = new System.Windows.Forms.Label();
            this.vehicleNumberLabel = new System.Windows.Forms.Label();
            this.vehicleNumberTextBox = new System.Windows.Forms.TextBox();
            this.resultListView = new System.Windows.Forms.ListView();
            this.PropertyColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ValueColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.errorLabel = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // bayarButton
            // 
            this.bayarButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bayarButton.Location = new System.Drawing.Point(117, 330);
            this.bayarButton.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.bayarButton.Name = "bayarButton";
            this.bayarButton.Size = new System.Drawing.Size(150, 50);
            this.bayarButton.TabIndex = 2;
            this.bayarButton.Text = "&Bayar";
            this.bayarButton.UseVisualStyleBackColor = true;
            this.bayarButton.Visible = false;
            this.bayarButton.Click += new System.EventHandler(this.bayarButton_Click);
            // 
            // cariButton
            // 
            this.cariButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cariButton.Location = new System.Drawing.Point(143, 100);
            this.cariButton.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.cariButton.Name = "cariButton";
            this.cariButton.Size = new System.Drawing.Size(99, 35);
            this.cariButton.TabIndex = 4;
            this.cariButton.Text = "&Cari";
            this.cariButton.UseVisualStyleBackColor = true;
            this.cariButton.Click += new System.EventHandler(this.cariButton_Click);
            // 
            // backButton
            // 
            this.backButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.backButton.Location = new System.Drawing.Point(143, 412);
            this.backButton.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.backButton.Name = "backButton";
            this.backButton.Size = new System.Drawing.Size(99, 35);
            this.backButton.TabIndex = 5;
            this.backButton.Text = "&Main Menu";
            this.backButton.UseVisualStyleBackColor = true;
            this.backButton.Click += new System.EventHandler(this.backButton_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(384, 24);
            this.menuStrip1.TabIndex = 6;
            this.menuStrip1.Text = "menuStrip";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mainMenuToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // mainMenuToolStripMenuItem
            // 
            this.mainMenuToolStripMenuItem.Name = "mainMenuToolStripMenuItem";
            this.mainMenuToolStripMenuItem.ShortcutKeyDisplayString = "Alt + F4";
            this.mainMenuToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.mainMenuToolStripMenuItem.Text = "&Main Menu";
            this.mainMenuToolStripMenuItem.Click += new System.EventHandler(this.mainMenuToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.ShortcutKeyDisplayString = "";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.exitToolStripMenuItem.Text = "&Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "&Help";
            this.helpToolStripMenuItem.Click += new System.EventHandler(this.helpToolStripMenuItem_Click);
            // 
            // appNameLabel
            // 
            this.appNameLabel.AutoSize = true;
            this.appNameLabel.BackColor = System.Drawing.Color.WhiteSmoke;
            this.appNameLabel.Font = new System.Drawing.Font("Montserrat", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.appNameLabel.Location = new System.Drawing.Point(440, 0);
            this.appNameLabel.Name = "appNameLabel";
            this.appNameLabel.Size = new System.Drawing.Size(97, 22);
            this.appNameLabel.TabIndex = 10;
            this.appNameLabel.Text = "Parking App";
            // 
            // vehicleNumberLabel
            // 
            this.vehicleNumberLabel.AutoSize = true;
            this.vehicleNumberLabel.Location = new System.Drawing.Point(110, 33);
            this.vehicleNumberLabel.Name = "vehicleNumberLabel";
            this.vehicleNumberLabel.Size = new System.Drawing.Size(165, 22);
            this.vehicleNumberLabel.TabIndex = 11;
            this.vehicleNumberLabel.Text = "ID / Nomor Kendaraan";
            // 
            // vehicleNumberTextBox
            // 
            this.vehicleNumberTextBox.Font = new System.Drawing.Font("Montserrat Light", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vehicleNumberTextBox.Location = new System.Drawing.Point(107, 58);
            this.vehicleNumberTextBox.Name = "vehicleNumberTextBox";
            this.vehicleNumberTextBox.Size = new System.Drawing.Size(170, 34);
            this.vehicleNumberTextBox.TabIndex = 12;
            this.vehicleNumberTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.vehicleNumberTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.vehicleNumberTextBox_KeyDown);
            // 
            // resultListView
            // 
            this.resultListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.PropertyColumnHeader,
            this.ValueColumnHeader});
            this.resultListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.resultListView.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem19,
            listViewItem20,
            listViewItem21,
            listViewItem22,
            listViewItem23,
            listViewItem24});
            this.resultListView.Location = new System.Drawing.Point(12, 169);
            this.resultListView.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.resultListView.Name = "resultListView";
            this.resultListView.Size = new System.Drawing.Size(360, 151);
            this.resultListView.TabIndex = 13;
            this.resultListView.UseCompatibleStateImageBehavior = false;
            this.resultListView.View = System.Windows.Forms.View.Details;
            this.resultListView.Visible = false;
            // 
            // PropertyColumnHeader
            // 
            this.PropertyColumnHeader.Text = "";
            this.PropertyColumnHeader.Width = 175;
            // 
            // ValueColumnHeader
            // 
            this.ValueColumnHeader.Text = "";
            this.ValueColumnHeader.Width = 155;
            // 
            // errorLabel
            // 
            this.errorLabel.AutoSize = true;
            this.errorLabel.Font = new System.Drawing.Font("Montserrat Light", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.errorLabel.Location = new System.Drawing.Point(20, 172);
            this.errorLabel.Name = "errorLabel";
            this.errorLabel.Size = new System.Drawing.Size(345, 34);
            this.errorLabel.TabIndex = 14;
            this.errorLabel.Text = "Kendaraan tersebut tidak ada.";
            this.errorLabel.Visible = false;
            // 
            // PaymentForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(384, 461);
            this.ControlBox = false;
            this.Controls.Add(this.errorLabel);
            this.Controls.Add(this.vehicleNumberLabel);
            this.Controls.Add(this.vehicleNumberTextBox);
            this.Controls.Add(this.appNameLabel);
            this.Controls.Add(this.backButton);
            this.Controls.Add(this.cariButton);
            this.Controls.Add(this.bayarButton);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.resultListView);
            this.Font = new System.Drawing.Font("Montserrat Light", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.KeyPreview = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PaymentForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Bayar - Parking App";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.PaymentForm_KeyDown);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button bayarButton;
        private System.Windows.Forms.Button cariButton;
        private System.Windows.Forms.Button backButton;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.Label appNameLabel;
        private System.Windows.Forms.Label vehicleNumberLabel;
        private System.Windows.Forms.TextBox vehicleNumberTextBox;
        private System.Windows.Forms.ListView resultListView;
        private System.Windows.Forms.ToolStripMenuItem mainMenuToolStripMenuItem;
        private System.Windows.Forms.ColumnHeader PropertyColumnHeader;
        private System.Windows.Forms.ColumnHeader ValueColumnHeader;
        private System.Windows.Forms.Label errorLabel;
    }
}