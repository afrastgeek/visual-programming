﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace NasabahDTO
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            // instankan NasabahDTO
            NasabahDTOx nb = new NasabahDTOx(
                txtNoRek.Text, txtNama.Text, txtAlamat.Text
            );

            // kirim ke repository
            NasabahREPO nr = new NasabahREPO(nb);
            
            // lakukan penyimpanan
            nr.simpan();

            MessageBox.Show(nr.getHasilSimpan());

        }
    }
}
