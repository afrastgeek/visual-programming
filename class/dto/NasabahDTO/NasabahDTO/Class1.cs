﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NasabahDTO
{
    // Ini hanya berisi atribut yang diperlukan saja.
    // Methodnya hanya get dan set.
    public class NasabahDTOx
    {
        public String noRekening { get; set; }
        public String nama { get; set; }
        public String alamat { get; set; }

        // Default constructor.
        public NasabahDTOx()
        {

        }

        // Constructor with parameter.
        public NasabahDTOx(String nr, String nm, String al)
        {
            noRekening = nr;
            nama = nm;
            alamat = al;
        }


    }

    /**
     * Pada class repository, atribut merupakan DTO
     * Fungsi memanggil atau mengaitkan antara DTO
     * dengan Database.
     */
    class NasabahREPO
    {
        NasabahDTOx dtNSB;
        // hanya untuk melihat hasil
        String argumenHasil;

        // Constructor.
        public NasabahREPO(NasabahDTOx xNasabah)
        {
            dtNSB = xNasabah;
        }

        public void simpan()
        {
            NasabahDB ndb = new NasabahDB();
            ndb.insertNewRecord(dtNSB);
            
            argumenHasil = "Berhasil disimpan! ";
            argumenHasil += ndb.getResponDB();
        }

        public String getHasilSimpan()
        {
            return argumenHasil + " " + dtNSB.nama;
        }
    }

    /**
     * Silakan masukkan atribut dan method
     * selengkap-lengkapnya.
     */
    class NasabahDB
    {
        String noRekening { get; set; }
        String nama { get; set; }
        String alamat { get; set; }
        String noTelp { get; set; }
        Double saldo { get; set; }
        String email { get; set; }
        bool statusKawin { get; set; }

        String responDB;


        // Default constructor.
        public NasabahDB()
        {

        }

        // Constructor with parameter.
        public NasabahDB(String nr, String nm, String al)
        {
            // nanti lengkapi
            noRekening = nr;
            nama = nm;
            alamat = al;
        }

        public void bukaKoneksi()
        {

        }

        public void tutupKoneksi()
        {

        }

        public bool insertNewRecord(NasabahDTOx nb)
        {
            // buka koneksi
            bukaKoneksi();

            // masukkan sql
            // insert into nasabah (...) values (...)

            // tutup koneksi
            tutupKoneksi();

            responDB = "DTO sudah sampai ke database.";

            return true;
        }

        public String getResponDB()
        {
            return responDB;
        }

        public NasabahDTOx cariByID(int id)
        {
            NasabahDTOx retVal = null;

            // buka koneksi
            bukaKoneksi();

            // masukkan query
            // dicari berdasarkan ID
            // masukkan ke retVal

            // tutup koneksi
            tutupKoneksi();

            return retVal;
        }
    }
}
