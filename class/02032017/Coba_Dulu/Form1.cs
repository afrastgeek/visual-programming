﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Coba_Dulu
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        decimal fTD(string JN)
        {
            decimal hasil;
            switch (JN)
            {
                case "A":
                    hasil = 5000;
                    break;
                case "B":
                    hasil = 10000;
                    break;
                case "C":
                    hasil = 15000;
                    break;
                case "D":
                    hasil = 20000;
                    break;

                default:
                    hasil = 0;
                    break;


            }
            return hasil;

        }

        decimal fTB(decimal berat, decimal TD)
        {
            decimal hasil;

            hasil = berat * TD;

            if (berat > 50 && berat <= 100)
            {
                hasil = (50 * TD) + ((berat - 50) * 3 / 2 * TD);
            }
            else if (berat > 50 && berat <= 100)
            {
                hasil = (50 * TD) + ((berat - 50) * 3 / 2 * TD);
            }

            return hasil;
        }



        private void button1_Click(object sender, EventArgs e)
        {
            string jenis;
            jenis = Jeniscbo.Text;

            decimal TD;
            TD = fTD(jenis);
            TDtxt.Text = TD.ToString();

            decimal berat;
            berat = Convert.ToDecimal(Berattxt.Text);

            decimal bayar;
            bayar = fTB(berat, TD);
            TBtxt.Text = bayar.ToString();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            int A;
            int B;
            int C;

            A = 20;
            B = A++;
            C = ++A;

            MessageBox.Show(B.ToString() + " " + C.ToString());



        }

        private void button3_Click(object sender, EventArgs e)
        {
            string[] Mhs = { "Ali", "Budi", "Cecep" };
            int[] Nilai = new int[5];

            Nilai[0] = 80;
            Nilai[3] = 100;

            foreach (string quas in Mhs)
            {
                MessageBox.Show(quas);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Fiko_Ngobrol_Wae f1 = new Fiko_Ngobrol_Wae();
            f1.inisial(5, 10);

        }

        private void button5_Click(object sender, EventArgs e)
        {
            ListViewItem elemen = new ListViewItem(Berattxt.Text);

            elemen.SubItems.Add(Jeniscbo.Text);
            elemen.SubItems.Add(TDtxt.Text);
            elemen.SubItems.Add(TBtxt.Text);

            listView1.Items.Add(elemen);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            // membuat objek StringBuilder, nantinya diisi data dari ListView
            StringBuilder data = new StringBuilder();

            // ambil data dari ListView
            ListView.ListViewItemCollection all_item = listView1.Items;
            int num_column = listView1.Columns.Count;

            // untuk setiap baris di ListView
            foreach (ListViewItem item in all_item)
            {
                // untuk setiap kolom dari baris
                for (int i = 0; i < num_column; i++)
                {
                    // tambahkan ke objek StringBuilder, dipisahkan oleh spasi
                    data.Append(item.SubItems[i].Text + " ");
                }
                data.AppendLine();
            }

            // Buat Dialog Save file, disini kita mencoba mendapatkan nama file dari user
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "Text File|*.txt";
            saveFileDialog1.Title = "Save to a Text File";
            saveFileDialog1.ShowDialog();

            if (saveFileDialog1.FileName != "")
            {
                // Tulis data ke file
                File.WriteAllText(saveFileDialog1.FileName, data.ToString());
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            // Buat Dialog Open File, disini kita mencoba mendapatkan nama file dari user
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "Text File|*.txt";
            openFileDialog1.Title = "Open a Text File";
            openFileDialog1.ShowDialog();

            if (openFileDialog1.FileName != "")
            {
                // Baca file dan simpan textnya di variabel data
                using (StringReader data = new StringReader(File.ReadAllText(openFileDialog1.FileName)))
                {
                    string row;
                    while ((row = data.ReadLine()) != null)
                    {
                        string[] columns = row.Split(' ');

                        // buat objek ListViewItem, diisi dengan kolom pertama
                        ListViewItem lvi = new ListViewItem(columns[0]);

                        // isi dengan kolom selanjutnya
                        for (int i = 1; i < columns.Count(); i++)
                        {
                            lvi.SubItems.Add(columns[i]);
                        }

                        // tambahkan objek ListViewItem ke ListView
                        listView1.Items.Add(lvi);
                    }
                }
            }
        }
    }
}
